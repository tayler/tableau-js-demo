# Demo: HTML, CSS, JS, and Tableau JS API basics

## Demo steps
HTML files build on each other, first introducing HTML, then CSS, then
JavaScript, then the Tableau JS API. base.html, css.html, js.html have  corresponding *-complete.html files that show what the file looked like
after adding on to it. Files go in this order:
- base.html -> css.html -> js.html